package com.cxgc.thymeleaftest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.cxgc.thymeleaftest")
public class ThymeleaftestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThymeleaftestApplication.class, args);
    }

}
