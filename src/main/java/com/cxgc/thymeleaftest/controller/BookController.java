package com.cxgc.thymeleaftest.controller;

import com.cxgc.thymeleaftest.mapper.EmployeeMapper;
import com.cxgc.thymeleaftest.pojo.Book;
import com.cxgc.thymeleaftest.pojo.Employee;
import com.cxgc.thymeleaftest.pojo.Entry;
import com.sun.org.apache.xpath.internal.operations.Mod;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * All Rights Reserved, Designed By ZHB.
 *
 * @author:
 * @date:
 * @copyright:
 */
@Controller
public class BookController {

    @Autowired
    EmployeeMapper employeeMapper;

    @GetMapping("/books")
    public ModelAndView books() {
        List<Book> books = new ArrayList<>();
        Book b1 = new Book();
        b1.setAuthor("罗贯中");
        b1.setId(1);
        b1.setName("三国演义");
        books.add(b1);
        books.add(b1);
        ModelAndView mv = new ModelAndView();
        mv.addObject("books", books);
        mv.addObject("mybook", b1);
        mv.setViewName("book-view");
        return mv;

    }

    @GetMapping("/")
    public ModelAndView formStudy() {
        ModelAndView mv = new ModelAndView();
        List<Entry> entries = new ArrayList<>();
        Entry entry = new Entry();
        entry.setText("textha");
        entry.setTitle("titlehaha");
        entries.add(entry);
        mv.addObject("entries", entries);
        mv.addObject("command", entry);

        mv.setViewName("form-study");

        return mv;
    }

    @PostMapping("/entry")
    public void post(Entry entry) {
        System.out.println(entry.toString());


    }

    @GetMapping("/list")
    public ModelAndView listEmployees() {
        List<Employee> employees = employeeMapper.selectList(null);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("employees", employees);
        modelAndView.setViewName("employee-view");

        return modelAndView;

    }

    @GetMapping("/form")
    public String showAddForm(Model model) {
        model.addAttribute("employee", new Employee());
        return "form";

    }

    @PostMapping("/save")
    public String saveEmployee(@ModelAttribute("employee") Employee employee) {
        System.out.println(employee.toString());
        employeeMapper.updateById(employee);
        return "redirect:/list";
    }

    @GetMapping("/update")
    public String showUpdateForm(@RequestParam("employeeId") String id, Model model) {
        Employee employee = employeeMapper.selectById(id);
        model.addAttribute("employee", employee);
        return "update-employee";
    }

    @GetMapping("/delete")
    public String deleteEmployee(@RequestParam("employeeId") String id, Model model) {
        employeeMapper.deleteById(id);
        return "redirect:/list";
    }
}
