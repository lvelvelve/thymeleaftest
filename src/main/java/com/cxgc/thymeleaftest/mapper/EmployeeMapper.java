package com.cxgc.thymeleaftest.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxgc.thymeleaftest.pojo.Employee;

import org.springframework.stereotype.Repository;

/**
 * All Rights Reserved, Designed By ZHB.
 *
 * @author:
 * @date:
 * @copyright:
 */
@Repository
public interface EmployeeMapper extends BaseMapper<Employee> {

}
