package com.cxgc.thymeleaftest.pojo;

import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * All Rights Reserved, Designed By ZHB.
 *
 * @author:
 * @date:
 * @copyright:
 */
@Data
@NoArgsConstructor
public class Employee {
    private String firstName;

    private String lastName;

    private String email;

    @TableId
    private Long id;
}
