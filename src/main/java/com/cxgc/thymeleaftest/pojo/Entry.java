package com.cxgc.thymeleaftest.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * All Rights Reserved, Designed By ZHB.
 *
 * @author:
 * @date:
 * @copyright:
 */
@Data
@NoArgsConstructor
public class Entry {

    private String title;

    private String text;
}
